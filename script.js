let quizData = [
    {
        question : 'Quel âge a jérémy ?',
        a: '10',
        b: '33',
        c: '27',
        d: '189',
        correct: 'c'
    }, {
        question:'Quel est le meilleur langage de programmation ?',
        a: 'Java',
        b: 'C',
        c: 'Python',
        d: 'Javascript',
        correct: 'd'
    }, {
        question: 'Qui est le président des Etats-Unis',
        a: 'Florin Pop',
        b: 'Donald Trump',
        c: 'Obiwen Kenoby',
        d: 'Barack Obama',
        correct: 'b'
    }, {
        question: 'Que signifie HTML ?',
        a: 'Hypertext Markup Language',
        b: 'Cascading Style Sheet',
        c: 'Jason Object Notation',
        d: 'Helicopters Terminals Motorboats Lamborginis',
        correct: 'a'
    }, {
        question: 'En quelle année Javascript a-t-\'il été crée ?',
        a: '1996',
        b: '1995',
        c: '1944',
        d: 'Aucunes de ces dates.',
        correct: 'd'
    }
];

let questionEl = document.getElementById('question');
let a_text = document.getElementById('a_text');
let b_text = document.getElementById('b_text');
let c_text = document.getElementById('c_text');
let d_text = document.getElementById('d_text');
let submitBtn = document.getElementById('submit');

let currentQuiz = 0;

loadQuizz();

function loadQuizz(){
    let currentQuizData = quizData[currentQuiz];

    questionEl.innerText=currentQuizData.question;

    a_text.innerText = currentQuizData.a;
    b_text.innerText = currentQuizData.b;
    c_text.innerText = currentQuizData.c;
    d_text.innerText = currentQuizData.d;

    currentQuiz++;
}

submitBtn.addEventListener('click', () =>{
    currentQuiz++;

    if(currentQuiz < quizData.length){
        loadQuizz();
    }else{
        // show result
        alert("Le test est finit, vous pouvez aller chercher une limonade !");
    }
})